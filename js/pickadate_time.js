/**
 * @file
 * The main javascript file for the datetime module
 *
 * @ingroup datetime
 * @{
 */

 jQuery( document ).ready(function() {
  /**
  * @} Start of "loading modal".
  */

	 jQuery( '.datepicker' ).pickadate({
	 		format: 'yyyy-mm-dd',	
			formatSubmit: 'yyyy-mm-dd',
			labelMonthNext: 'Go to the next month',
			labelMonthPrev: 'Go to the previous month',
			labelMonthSelect: 'Pick a month from the dropdown',
			labelYearSelect: 'Pick a year from the dropdown',
			selectMonths: true,
			selectYears: true,
			closeOnSelect: true,
			closeOnClear: false,
			selectYears: 4,				
	})
	var picker_from = jQuery('#input_01').pickadate('picker');
	if(picker_from){
		picker_from.set('select', {format: 'yyyy-mm-dd' });	   
	}    

    var $input_time = jQuery( '.timepicker' ).pickatime({});
    var picker = $input_time.pickatime('picker');

});
