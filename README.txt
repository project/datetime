INTRODUCTION
------------

Date time picker form element using the pickadate.js Plugin and create a custom
field type of Date and Time Picker. So that anyone can easily add date and
time or datetime picker.

It supports date/time and date fields. You can use it on Datetime and Date
fields.

REQUIREMENTS
------------

This module not require other modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
